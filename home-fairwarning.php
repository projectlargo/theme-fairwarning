<?php get_header(); ?>

			<div id="content" class="stories span8" role="main">

			<?php
				global $ids;
				$ids = array();

				// get the optional homepage top section (if set)
				if (of_get_option('homepage_layout') == 'topstories') {
					get_template_part( 'home-part-topstories' );
				} else if (of_get_option('homepage_layout') == 'slider') {
					get_template_part( 'home-part-slider' );
				}

				$args = array(
					'paged'			=> $paged,
					'posts_per_page'=> 10,
					'cat'			=> 17,
					'post__not_in' 	=> $ids

				);
				$query = new WP_Query( $args );

				if ( $query->have_posts() ) {
						while ( $query->have_posts() ) : $query->the_post(); $ids[] = get_the_ID(); ?>

						<article id="post-<?php the_ID(); ?>">
							<header>
					 			<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="Permalink to <?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					 			<div class="post-meta">
					 				<h5 class="byline"><?php largo_byline(); ?><?php edit_post_link('Edit This Post', ' | <span class="edit-link">', '</span>'); ?></h5>
					 			</div>
							</header><!-- / entry header -->

							<div class="entry-content">
								<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
								<?php largo_excerpt( $post, 5, '', 0); ?>
					        	<?php if ( largo_has_categories_or_tags() ): ?>
					            	<div class="post-meta bottom-meta">
					    				 <h5>
					    				 	<strong>Filed under:</strong>
						    				 	<?php echo largo_homepage_categories_and_tags( 5 ); ?>

					    				 </h5>
					            	</div><!-- /.post-meta -->
					            <?php endif; ?>
							</div><!-- .entry-content -->

						</article><!-- #post-<?php the_ID(); ?> -->
			<?php endwhile;
				largo_content_nav( 'nav-below' );
			 } else { ?>

				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title">Nothing Found</h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p>Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.</p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php } ?>

			</div><!--/.grid_8 #content-->

			<div id="sidebar" class="span4">
				<?php get_sidebar(); ?>
			</div><!-- /.grid_4 -->
<?php get_footer(); ?>